#include "../HpInclude/HPSocket4C.h"
#include <stdio.h>

En_HP_HandleResult __stdcall OnConnect(HP_Client pSender, HP_CONNID dwConnID)
{
	TCHAR szAddress[50];
	int iAddressLen = sizeof(szAddress) / sizeof(TCHAR);
	USHORT usPort;

	::HP_Client_GetLocalAddress(pSender, szAddress, &iAddressLen, &usPort);

	return HR_OK;
}

En_HP_HandleResult __stdcall OnSend(HP_Server pSender, HP_CONNID dwConnID, const BYTE* pData, int iLength)
{
	return HR_OK;
}

En_HP_HandleResult __stdcall OnReceive(HP_Server pSender, HP_CONNID dwConnID, const BYTE* pData, int iLength)
{
	printf("receive data [connID=%d]:%s\n", dwConnID, pData);
	::HP_Server_Send(pSender, dwConnID, pData, iLength);
	return HR_OK;
}

En_HP_HandleResult __stdcall OnClose(HP_Server pSender, HP_CONNID dwConnID, En_HP_SocketOperation enOperation, int iErrorCode)
{
	printf("[connID=%d] closed\n", dwConnID);
	return HR_OK;
}

int main()
{
	HP_TcpPackClient m_pClient;
	HP_TcpPackClientListener m_pListener;

	// 创建监听器对象
	m_pListener = ::Create_HP_TcpPackClientListener();
	// 创建 Socket 对象
	m_pClient = ::Create_HP_TcpPackClient(m_pListener);
	// 设置 Socket 监听器回调函数
	::HP_Set_FN_Client_OnConnect(m_pListener, OnConnect);
	::HP_Set_FN_Client_OnSend(m_pListener, OnSend);
	::HP_Set_FN_Client_OnReceive(m_pListener, OnReceive);
	::HP_Set_FN_Client_OnClose(m_pListener, OnClose);

	HP_TcpPackClient_SetMaxPackSize(m_pClient, 0x0FFF);
	HP_TcpPackClient_SetPackHeaderFlag(m_pClient, 0x169);

	if (::HP_Client_Start(m_pClient, (LPCTSTR)"127.0.0.1", 6000, false))
	{
		printf("connect to server successfully\n");
	}
	else
	{
		printf("connect to server failed\n");
	}
	const char* msg = "hello world";
	::HP_Client_Send(m_pClient, (const BYTE*)msg, strlen(msg));

	getchar();
}