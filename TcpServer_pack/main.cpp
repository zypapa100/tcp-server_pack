#include "../HpInclude/HPSocket4C.h"
#include <stdio.h>

En_HP_HandleResult __stdcall OnPrepareListen(HP_Server pSender, SOCKET soListen)
{
	TCHAR szAddress[50];
	int iAddressLen = sizeof(szAddress) / sizeof(TCHAR);
	USHORT usPort;

	::HP_Server_GetListenAddress(pSender, szAddress, &iAddressLen, &usPort);
	return HR_OK;
}

En_HP_HandleResult __stdcall OnAccept(HP_Server pSender, HP_CONNID dwConnID, SOCKET soClient)
{
	BOOL bPass = TRUE;
	TCHAR szAddress[50];
	int iAddressLen = sizeof(szAddress) / sizeof(TCHAR);
	USHORT usPort;

	::HP_Server_GetRemoteAddress(pSender, dwConnID, szAddress, &iAddressLen, &usPort);

	//const char* msg = "hello worldd";
	char msg[256] = { 0 };
	::HP_Server_Send(pSender, dwConnID, (const BYTE*)msg, 256);

	printf("%s:%d connected...\n", szAddress, usPort);
	return bPass ? HR_OK : HR_ERROR;
}

En_HP_HandleResult __stdcall OnSend(HP_Server pSender, HP_CONNID dwConnID, const BYTE* pData, int iLength)
{
	return HR_OK;
}

En_HP_HandleResult __stdcall OnReceive(HP_Server pSender, HP_CONNID dwConnID, const BYTE* pData, int iLength)
{
	printf("receive data [connID=%d]:%s\n", dwConnID, pData);
	::HP_Server_Send(pSender, dwConnID, pData, iLength);
	return HR_OK;
}

En_HP_HandleResult __stdcall OnClose(HP_Server pSender, HP_CONNID dwConnID, En_HP_SocketOperation enOperation, int iErrorCode)
{
	printf("[connID=%d] closed\n", dwConnID);
	return HR_OK;
}

En_HP_HandleResult __stdcall OnShutdown(HP_Server pSender)
{
	return HR_OK;
}


int main()
{
	HP_TcpServer m_pServer;
	HP_TcpServerListener m_pListener;

	// 创建监听器对象
	m_pListener = ::Create_HP_TcpPackServerListener();
	// 创建 Socket 对象
	m_pServer = ::Create_HP_TcpPackServer(m_pListener);

	// 设置 Socket 监听器回调函数
	::HP_Set_FN_Server_OnPrepareListen(m_pListener, OnPrepareListen);
	::HP_Set_FN_Server_OnAccept(m_pListener, OnAccept);
	::HP_Set_FN_Server_OnSend(m_pListener, OnSend);
	::HP_Set_FN_Server_OnReceive(m_pListener, OnReceive);
	::HP_Set_FN_Server_OnClose(m_pListener, OnClose);
	::HP_Set_FN_Server_OnShutdown(m_pListener, OnShutdown);

	// 设置包头标识与最大包长限制
	::HP_TcpPackServer_SetMaxPackSize(m_pServer, 0xFFF);
	::HP_TcpPackServer_SetPackHeaderFlag(m_pServer, 0x169);

	if (::HP_Server_Start(m_pServer, "127.0.0.1", 6000))
	{
		printf("start tcp server successfully\n");
	}
	else
	{
		printf("start tcp server failed\n");
		return 0;
	}
	getchar();
}